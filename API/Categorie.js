export  function getAllCategorie() {
    const url = 'http://192.168.43.29/wsFoot/public/api/list/categorie'
    return fetch(url, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
    })
    .then((response) => response.json())
    .then ((json) => {
        console.log(json);
    })
    .catch((error) => console.error(error))
}
