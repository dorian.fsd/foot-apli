import 'react-native-gesture-handler';
import React, {Component} from 'react';
import { Button,Text, View} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Login from './Components/Login';
import AccueilDorian from './Components/AccueilDorian';
import { createStackNavigator } from '@react-navigation/stack';


function Connexion() {
  return (
    <Login/>
  )
}
function HomeDorian ({}) {
  return(
    <AccueilDorian/>
  )
}
function Home({ navigation }) { 
  return (
    <View>
      <Text>Bonjour je suis à l'accueil</Text>
    </View>
  )
}
const Stack = createStackNavigator();


function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="accueil" component={HomeDorian}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App;
// class App extends React.Component{

//   /*constructor(props) {
//     super(props);

//     this.state = {
//       connect: props.connect,
//     };
//   }*/
// }
//   render() {

//     return (
//       <NavigationContainer>
//         <Stack.Navigator>
//           <Stack.Screen
//             name="Home"
//             component={HomeScreen}
//             options={{ title: 'Welcome' }}
//           />
//           <Stack.Screen
//             name="Acueil"
//             component={HomeAccueil}
//             options={{ title: 'Accueil' }}
//           />
//           <Stack.Screen 
//             name="Login" 
//             component={Connexion} />
//         </Stack.Navigator>
//       </NavigationContainer>
//     );
//   }
// }

// const HomeScreen = ({ navigation }) => {
//   return (
//     <Button
//       title="Aller à la page login"
//       onPress={() =>
//         navigation.navigate('Login')
//       }
//       style={{width: 50}}
//     />
//   );
// };
// const HomeAccueil = ({ navigation }) => {
//   return (
//     <Button
//       title="Aller à la page Accueil"
//       onPress={() =>
//         navigation.navigate('Accueil')
//       }
//       style={{width: 50}}
//     />
//   );
// };
// function Connexion(){
//   return (
//     <Login />)
// }

// export default App;