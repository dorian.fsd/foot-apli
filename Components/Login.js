import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { View,TextInput,StyleSheet,Text,Image,TouchableOpacity} from 'react-native';
import  { getUser } from '../API/Connexion'

class Login extends React.Component{

    // Les props de ma classe
    constructor(props) {
        super(props)
        /*this.email = "root@gmail.com"
        this.password = ""*/
        this.state = {
            email : '',
            password : '',
            isLoading : false,
            connect : false
        }
    }

    // Méthode pour se connecter à l'API
    _login() {
        if (this.state.email.length > 0 && this.state.password.length > 0 ) {
            this.setState({isLoading : true})
            getUser(this.state.email,this.state.password).then(data => {
                if (data.connected == true ) {
                    this.setState({
                        connect : true,
                        isLoading : false
                    })
                    this.props.navigation.navigate('App');
                    console.log(this.state.connect)
                }
                else {
                    this.setState({ isLoading : false})
                    console.log("Pas connecté")
                }
            })
        }
        else if (this.state.email.length == 0 && this.state.password.length == 0 ) {
            console.log("Rempli les champs bordel")
        }
    }

    //Méthode pour afficher une vue pendant que le user se connecte
    _displayLoading() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading_container}>
                    <Text>Salut tu est en train de te connecter à l'api :p</Text>
                </View>
            )
        }
    }

    _submit() {
        console.warn(this.state.email,this.state.password)
    }


    // Le rendu de ma vue sur mon application
    render() {
        return(
            <View style={styles.container}>
                <Image 
                    source={require('../assets/logo.png')}
                    style={styles.image}
                />
                <Text style={styles.titrelogin}><Icon name="user" size={25}/> Nom d'utilisateur</Text>
                <TextInput 
                placeholder='Login'
                onChangeText={(text) => {this.setState({email : text}) }}
                style={styles.textinputlogin}
                />
                <Text style={styles.titremdp}><Icon name="key" size={25}/> Mot de passe</Text>
                <TextInput 
                placeholder='Password'
                onChangeText={(text) => {this.setState({password : text}) }}
                //onSubmitEditing={() => this._login()}
                style={styles.textinputmdp}
                />
                <TouchableOpacity
                    onPress={
                        () => {this._login()}
                    }
                    style={styles.button}
                >
                    <Text style={styles.buttonmessage}>Connexion</Text>
                </TouchableOpacity>
                {this._displayLoading()}
            </View>
        )
    }
}


// Tout le style de la vue
const styles = StyleSheet.create({
    container: {
        width: 300,
        flex: 1,
        padding: 10,
    },
    image: {
        width: 50,
        height: 50,
        marginTop: 15,
        marginLeft: 150,
    },
    titrelogin: {
        marginTop: 30,
        marginLeft: 80,
        fontSize: 20,
    },
    textinputlogin: {
        marginTop: 20,
        borderBottomWidth: 1,
        fontSize: 15,
        width: 150,
        marginLeft: 100,
        textAlign: "center",
    },
    titremdp: {
        marginTop: 40,
        marginLeft: 80,
        fontSize: 20,
    },
    textinputmdp:{
        marginTop: 20,
        borderBottomWidth: 1,
        fontSize: 15,
        width: 150,
        marginLeft: 100,
        textAlign: "center",
    },
    button: {
        alignItems: "center",
        marginTop: 35,
        marginLeft: 60,
        backgroundColor: '#00A6FF'
    },
    buttonmessage: {
        padding: 5,
        textAlign: "center",
        color: '#fff'
    },
})

export default Login;