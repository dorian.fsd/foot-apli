import React from 'react';
import {Text, View, FlatList, Button, TextInput} from 'react-native';
import CategoriesItem from'./CategoriesItem' ;
import  { getAllCategorie } from '../API/Categorie'


class AccueilDorian extends React.Component {

    constructor(props){
        super(props)
        this.state = {categories : [],
        }
        this.searchedCategories = ""
    }
    

/*_loadCategories(){
 if (this.searchedCategories.lenght > 0){
    getAllCategorie(this.searchedCategories).then(data => this.setState( {categories : data.results }))
 }
}*/
_loadCategories() {
    getAllCategorie();
}
_searcheTextInputChanged(text){
    this.searchedCategories = text
}

    render() {
        return(
            <View>
                <TextInput onChangeText={(text) => this._searcheTextInputChanged(text)} placeholder="Categorie"/>
                <Button style = {{ height: 50}} title = "rechercher" onPress={()=>this._loadCategories()}/>
                <FlatList
                 data={this.state.categories}
                 renderItem={({item}) => <CategoriesItem/>}
                />
            </View>
        )
    }
}



export default AccueilDorian;